from django.db import models

# Create your models here.
from profiles.models import GameUser


class Game(models.Model):
    name = models.CharField(max_length=200)
    date_borrowed = models.DateTimeField('date borrowed')
    reserved = models.BooleanField(default=False)
    borrower = models.ForeignKey(GameUser, on_delete=models.SET_NULL, null=True, blank=True, default=None)
